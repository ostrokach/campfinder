import os
from typing import List

from dotenv import load_dotenv

load_dotenv(dotenv_path=os.getenv("ENV_FILE", ".env"))

# Authentication for sending emails
EMAIL_HOST: str = os.getenv("EMAIL_HOST", "smtp.gmail.com")
EMAIL_PORT: int = int(os.getenv("EMAIL_PORT", "587"))
EMAIL_USERNAME: str = os.getenv("EMAIL_USERNAME", "")
EMAIL_PASSWORD: str = os.getenv("EMAIL_PASSWORD", "")
EMAIL_USE_TLS: bool = os.getenv("EMAIL_USE_TLS", "").lower() == "true"
EMAIL_RECIPIENTS: List[str] = os.getenv("EMAIL_RECIPIENTS", "").split(",")

CHROME_EXECUTABLE_PATH: str = os.getenv("CHROME_EXECUTABLE_PATH", "chromedriver")
