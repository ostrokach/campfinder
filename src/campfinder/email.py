import logging
import smtplib
from email.message import EmailMessage

from campfinder import config

logger = logging.getLogger(__name__)


def send_email(available_sites_df):
    msg = EmailMessage()
    msg["Subject"] = "[ontario parks] Camping availability"
    msg["From"] = config.EMAIL_USERNAME
    msg["To"] = ", ".join(config.EMAIL_RECIPIENTS)
    msg.set_content(available_sites_df.to_csv(index=False))
    msg.add_alternative(available_sites_df.to_html(index=False), subtype="html")

    logger.debug("Sending email")
    try:
        with smtplib.SMTP(config.EMAIL_HOST, config.EMAIL_PORT) as s:
            if config.EMAIL_USE_TLS:
                s.starttls()
            if config.EMAIL_USERNAME and config.EMAIL_PASSWORD:
                s.login(config.EMAIL_USERNAME, config.EMAIL_PASSWORD)
            s.send_message(msg)
        logger.info("Email sent successfully! :)")
    except Exception as e:
        error_msg = f"The following exception occured while trying to send mail: {e!r}"
        logger.error(error_msg)
        raise
