import logging
from datetime import datetime

import pandas as pd

from campfinder import browser, email, parks_ontario

logger = logging.getLogger(__name__)


def check_availability(driver, urls: str, start_date: str, end_date: str) -> pd.DataFrame:
    nights = (datetime.fromisoformat(end_date) - datetime.fromisoformat(start_date)).days
    urls = urls.format(start_date=start_date, end_date=end_date, nights=nights)

    dfs = []
    for url in urls.split(","):
        logger.info("Downloading table from url: %s", url)
        table_src = parks_ontario.download_availability_table(driver, url)
        df = parks_ontario.parse_availability_table(table_src)
        try:
            df = parks_ontario.filter_by_dates(df, start_date, end_date, nights)
        except IndexError as e:
            print(f"Error: {e!r}")
            logger.error("table_src: %s", table_src)
            logger.error("df: %s", df)
            raise
        df["url"] = url
        dfs.append(df)
        logger.info("Finished downloading table")
    return pd.concat(dfs).reset_index()


def main(urls: str, start_date: str, end_date: str) -> pd.DataFrame:
    driver = browser.start_chrome_driver()
    try:
        return check_availability(driver, urls, start_date, end_date)
    finally:
        driver.quit()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--urls",
        help="Comma-separated list of URLs",
    )
    parser.add_argument("--start-date")
    parser.add_argument("--end-date")
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="Verbosity (-v, -vv, etc.)",
    )
    args = parser.parse_args()

    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    logging.basicConfig(level=levels[args.verbose])

    args = parser.parse_args()

    available_sites_df = main(args.urls, args.start_date, args.end_date)

    if not available_sites_df.empty:
        email.send_email(available_sites_df)
