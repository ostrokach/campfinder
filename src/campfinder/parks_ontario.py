import re
import time

import pandas as pd
import selenium


def download_availability_table(wd, url):
    wd.get(url)
    grid_view_button = wd.find_element_by_id("grid-view-button")
    wd.execute_script("arguments[0].click();", grid_view_button)

    n_tries = 0
    while True:
        time.sleep(8)
        grid_table = wd.find_element_by_id("grid-table")
        try:
            table_src = "<table>" + grid_table.get_attribute("innerHTML") + "</table>"
            break
        except selenium.common.exceptions.StaleElementReferenceException as e:
            print(f"Error: {e!r}")
            n_tries += 1
            if n_tries > 5:
                raise
    return table_src


def parse_availability_table(table_src):
    result = pd.read_html(table_src)
    df_ref = result[0]

    table_src_clean = re.sub(
        '<td.+?class="(grid-u?n?available)?.+?td>',
        r"<td>\1</td>",
        table_src.replace("\n", ""),
    )
    result = pd.read_html(table_src_clean)
    df = result[0]

    df["Site"] = df_ref["Site"].str.findall("[0-9]+").str[0]
    df = df.set_index("Site")

    df.columns = (
        df.columns.str.replace("Your searched dates", "")
        .str.findall(r"([a-zA-Z\.]+) ([0-9]+)([a-zA-Z]+)")
        .str[0]
    )
    df = df.applymap(lambda x: pd.notnull(x) and "un" not in x)

    return df


def filter_by_dates(df, start_date, end_date, number_of_nights):
    assert int(df.columns[0][1]) == int(start_date.split("-")[-1])
    assert int(df.columns[number_of_nights][1]) == int(end_date.split("-")[-1])

    df = df.iloc[:, :number_of_nights]
    df = df[(df == True).all(axis=1)]

    return df
