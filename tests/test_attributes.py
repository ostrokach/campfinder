import pytest

import campfinder


@pytest.mark.parametrize("attribute", ["__version__"])
def test_attribute(attribute):
    assert getattr(campfinder, attribute)


def test_main():
    import campfinder

    assert campfinder
