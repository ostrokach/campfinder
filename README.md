# campfinder

[![gitlab](https://img.shields.io/badge/GitLab-main-orange?logo=gitlab)](https://gitlab.com/ostrokach/campfinder)
[![docs](https://img.shields.io/badge/docs-v0.1.2-blue.svg?logo=gitbook)](https://ostrokach.gitlab.io/campfinder/v0.1.2/)
[![conda](https://img.shields.io/conda/dn/ostrokach-forge/campfinder.svg?logo=conda-forge)](https://anaconda.org/ostrokach-forge/campfinder/)
[![pipeline status](https://gitlab.com/ostrokach/campfinder/badges/v0.1.2/pipeline.svg)](https://gitlab.com/ostrokach/campfinder/commits/v0.1.2/)
[![coverage report](https://gitlab.com/ostrokach/campfinder/badges/v0.1.2/coverage.svg?job=docs)](https://ostrokach.gitlab.io/campfinder/v0.1.2/htmlcov/)

Find cancelled campsites.
